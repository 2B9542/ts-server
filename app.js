const express = require("express");
const app = express();

const port = 5000;

app.use(express.json());

app.use(express.static("./public"));

/*
    HTTP CODES: GET, POST, PUT, DELETE

    GET: ceri o resursa
    POST: creezi o resursa
    PUT: actualizezi o resursa
    DELETE: stergi o resursa
*/

let songs = [
    {
        id: 0,
        name: "DATTEBAYO",
        released: 2020,
        artist: "NARUTOOOOO",
    },
    {
        id: 1,
        name: "DATTEBAYUU",
        released: 2022,
        artist: "SASKEEEEE",
    },
    {
        id: 2,
        name: "DATTEBACLOWN",
        released: 2018,
        artist: "HELO",
    }
];

app.get("/songs", (req, res) => {
    res.json(songs);
});

app.post("/songs", (req, res) => {
    const { song } = req.body;
    songs.push(song);
    res.sendStatus(200);
});

app.listen(port, () => {
    console.log("Listening on", port);
});