console.log("hello from client");
const container = document.getElementById("songs-container");
const songName = document.getElementById("song-name");
const artistName = document.getElementById("artist-name");
const released = document.getElementById("released");
const submit = document.getElementById("submit");

submit.addEventListener("click", ev => {
    postSong(getInput())
        .then(_ => {
            clearSongs();
            return getSongs();
        })
        .catch(err => console.error(err));
});

const today = new Date();
released.value = today.getFullYear();

getSongs()
    .then(_ => postSong(getInput()))
    .then(_ => {
        clearSongs();
        return getSongs();
    });

function clearSongs() {
    while(container.lastChild)
        container.removeChild(container.lastChild);
}

function getInput() {
    return {
        name: songName.value,
        artist: artistName.value,
        released: released.value,
    };
}

function getSongs() {
    return fetch("http://localhost:5000/songs")
        .then(response => response.json())
        .then(getElementsFromSongs)
        .then(elements => {
            elements.forEach(element => {
                container.appendChild(element);
            });
        })
        .catch(err => console.error(err));
}

function postSong(song) {
    return fetch("http://localhost:5000/songs", {
            method: "POST",
            headers: new Headers({
                // 'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            
            //make sure to serialize your JSON body
            body: JSON.stringify({
                song
            })
        })
        .then(response => console.log("Success"))
        .catch(error => console.error(error));
}

function getElementsFromSongs(songs) {
    return new Promise((resolve, reject) => {
        let elements = [];

        songs.forEach(song => {
            elements.push(createSongElement(song));
        });

        resolve(elements);
    });
}

function createSongElement(song) {
    let container = document.createElement("div");
    container.classList.add("col", "s12", "m6");
    let card = document.createElement("div");
    card.classList.add("card", "blue-grey", "darken-1");
    let cardContent = document.createElement("div");
    cardContent.classList.add("card-content", "white-text");
    let cardAction = document.createElement("div");
    cardAction.classList.add("card-action");
    let cardTitle = document.createElement("span");
    cardTitle.classList.add("card-title");
    cardTitle.innerText = song.name;
    let cardContentP = document.createElement("p");
    //  song.released.toString() + ", " + song.artist;
    cardContentP.innerText = `${song.released}, ${song.artist}`;

    cardContent.appendChild(cardTitle);
    cardContent.appendChild(cardContentP);
    card.appendChild(cardContent);
    card.appendChild(cardAction);
    container.appendChild(card);

    return container;
}